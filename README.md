# webpack-poc

A POC where i made the webpack setup from the ground up, so that i can understand the magic that happens in the modern web frameworks, like React and Vue.

## How to Run

1. Run git clone

- ssh: git clone `git@github.com:lucasmls/webpack-study.git`
- https: git clone `https://github.com/lucasmls/webpack-study.git`

2. Install the dependencies with `yarn`.
3. Run your application with `yarn start`.

- To build for development run: **yarn build:dev**
- To build for production run: **yarn build:prod**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
